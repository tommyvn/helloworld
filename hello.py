from wsgiref.simple_server import make_server, demo_app as app
import os

if __name__ == '__main__':
    port = int(os.getenv("PORT", "3333"))
    
    httpd = make_server('', port, app)
    print "Serving HTTP on port {}...".format(port)
    
    # Respond to requests until process is killed
    httpd.serve_forever()
    
    # Alternative: serve one request, then exit
    httpd.handle_request()
